# terradroid

Config + installs of Android apps I want... Because, FLOSS


## Usage

Eval the stuff in `core.clj` ...


## Todo

- things that i definitely need/want, but still require significant config
  - email
  - cal, contacts (need to sign in with google!)
  - syncthing
  - password store
  - openkeychain
  - muzei
  - not on f.droid:
      - maps.me

- evil things i still rely on:
  - telegram (not really FLOSS is it)
  - gmaps
  - play store
  - bing image of the day for muzei (probably harmless)
  - gsync for contacts, cal (move to etesync)

- backup these things:
  - dashclock
  - clock
  - uhabits

- secure config bacups
  - use cryptomator or something
  - syncthing (it has an "export fn")
  - freeotp (move from hacky bash script)
  - k9 mail


