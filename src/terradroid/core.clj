(ns terradroid.core
  (:gen-class)
  (:require [me.raynes.fs :as fs]
            [medley.core :as m]
            [taoensso.timbre :as log]
            [terradroid.apk :as apk]
            [terradroid.config :as config]
            [terradroid.repo :as repo]
            [terradroid.utils :as u :refer :all]))

(defn install-app
  [app-id]
  {:pre [(string? app-id)]}
  (log/info "==> installing " app-id)
  (if (u/downloaded? app-id)
    (let [{exit-code :exit :as res} (apk/install-app app-id)]
      (log/debug "==> done")
      (if (zero? exit-code) :ok (log/error res)))
    (log/error "no such file: " apk-path)))

(defn download-app
  ([repo app-id]
   (download-app repo app-id false))
  ([repo app-id force-redownload?]
   (let [dest (u/apk-path app-id)
         exists? (fs/exists? dest)
         download? (or (not exists?)
                       (and exists? force-redownload?))]
     (when download?
       (let [url (repo/apk-url repo app-id)]
         (log/info "downloading " url)
         (u/download url dest)
         (log/info "downloaded " url))))))

(defn download+install-app
  [repo app-id]
  (try
    (if (u/downloaded? app-id)
      (install-app app-id)
      (do (download-app repo app-id)
          (install-app app-id)))
    (catch Exception e
      (log/error e))))

(defn install-apps
  [app-ids]
  (doseq [id app-ids]
    (try
      (install-app id)
      (catch Exception e
        (log/error e)))))

(defn apk-urls
  "Return all the matching urls if you wanna batch download them or something."
  [repo app-ids]
  (let [in-app-ids? (set app-ids)]
    (->> repo
         (m/filter-keys #(in-app-ids? %)))))

(defn download-apks
  [repo app-ids]
  (let [repo (select-keys repo app-ids)
        futs (doall (for [app-id (keys repo)]
                      (future (download-app repo app-id))))
        deref+ (fn [fut]
                 (try
                   (deref fut)
                   (catch Exception ex
                     (log/error ex))))]
    (dorun (map deref+ futs))
    :ok))

(comment

  ;; FIXME mkdir .cache/terradroid
  (fs/mkdir "~/.cache/terradroid")
  (fs/mkdir "~/.cache/terradroid/apks")

  ;; FIXME errors are logged to the repl...

  (repo/local-idxs-clear config/config)

  (repo/config->repo config/config)

  (def repo (repo/config->repo config/config))

  (repo/download-apk repo "org.proninyaroslav.libretorrent")
  (install-app "org.proninyaroslav.libretorrent")

  (download+install-app repo "org.proninyaroslav.libretorrent")
  (download+install-app repo "org.fdroid.fdroid")
  (download+install-app repo "com.aurora.adroid")
  (download+install-app repo "com.aurora.store")

  (def apks-txt "poco_f3.txt")
  (def app-ids (u/resource-seq apks-txt))

  (download-apks repo app-ids)

  (install-apps app-ids)

  ;; TODO:
  ;; 0. WiFi: use QR code
  ;; 1. Syncthing -- wait for sync to finish
  ;; 2. OpenKeychain
  ;; 3. Password store
  ;; 3. AndOTP
  ;; - FireFox sync
  ;; - k9mail: import settings, refresh folders > generate passwords and fill them in;
  ;; - Davx: (cardDAV, calDAV);
  ;; - Simpletask: open todo.txt, import saved filters;
  ;; - Gadgetbridge
  ;; - System settings:
  ;;   - Battery: battery saver at 25%;
  ;;   - Display: scheduled dark mode, scheduled night light, adaptive brightness;
  ;;   - Sound: scheduled not disturb;
  ;;   - System:
  ;;     - Keyboard: italian, eng uk, turn off offensive word block;
  ;;     - Buttons: long press for torch, automatically turn off torch; 
  ;;     - Status bar: battery (circle, percentage), auto brightness;
  ;;     - Advanged:
  ;;       - Gestures: system navigation: gestures;
  ;;       - Backup: on;
  ;; - Maps: download world,ita,slo,austria;
  ;;;
  )
