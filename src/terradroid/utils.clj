(ns terradroid.utils
  (:require [clojure.java.io :as io]
            [terradroid.config :refer [config]]
            [me.raynes.fs :as fs]))

(defn resource-seq
  [filename]
  (->> filename
       io/resource
       io/reader
       line-seq
       (map clojure.string/trim)))

(defn download
  [url destination]
  (with-open [in (io/input-stream url)
              out (io/output-stream (io/file destination))]
    (io/copy in out)))

(defn apk-path
  [app-id]
  {:pre [(string? app-id)]}
  (fs/absolute
   (io/file (config :local-apks-path)
            (str app-id ".apk"))))

(defn downloaded? [app-id]
  (fs/exists? (apk-path app-id)))
