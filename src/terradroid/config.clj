(ns terradroid.config)

(def home (System/getProperty "user.home"))

(def config
  {:repos [{:name "f-droid"
            :idx/url "https://f-droid.org/repo/index.xml"
            ;; TODO: generate path from name
            :idx/path (str home "/.cache/terradroid/repo-index.xml")}
           {:name "izzy-on-droid"
            :idx/url "https://apt.izzysoft.de/fdroid/repo/index.xml"
            :idx/path (str home "/.cache/terradroid/repo-izzy-on-droid-index.xml")}]
   :local-apks-path (str home "/.cache/terradroid/apks/")})
