(ns terradroid.repo
  (:require [clojure.data.xml :as xml]
            [clojure.data.zip.xml :as zz]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [me.raynes.fs :as fs]
            [medley.core :as m]
            [taoensso.timbre :as log]
            [terradroid.config :refer [config]]
            [terradroid.utils :as utils]))

(defn apk-url
  [repo app-id]
  (get repo app-id))

(defn download-idx [idx-url path]
  (log/info "==> downloading index from" idx-url)
  (fs/mkdirs (fs/parent path))
  (utils/download idx-url path)
  :ok)

(defn download-idxs [config]
  (doseq [{:idx/keys [url path]} (:repos config)]
    (download-idx url path)))

(defn- application-nodes
  [index]
  (->> index
       :content
       (filter #(= :application (:tag %)))))
(defn- apk-tup
  [application-node]
  (let [apk-name (zz/xml1-> (zip/xml-zip application-node)
                            (zz/tag= :package)
                            (zz/tag= :apkname)
                            zz/text)
        apk-url (str "https://f-droid.org/repo/" apk-name)
        app-name (get-in application-node [:attrs :id])]
    [app-name apk-url]))
(defn- idx-path->repo
  [idx-path]
  (->> (application-nodes (-> idx-path
                              io/file
                              io/input-stream
                              xml/parse))
       (map apk-tup)
       (into {})))

(defn local-idxs-paths
  [config]
  (->> (config :repos)
       (map :idx/path)))

(defn local-idxs-exist?
  [config]
  (->> (config :repos)
       (map (comp fs/file :idx/path))
       (every? fs/exists?)))

(defn local-idxs-clear
  [config]
  (doseq [p (local-idxs-paths config)]
    (fs/delete p)))

(defn ->repo
  ([& idx-paths]
   (->> idx-paths
        (map idx-path->repo)
        (apply merge)
        doall)))

(defn config->repo
  [config]
  (let [idx-paths (local-idxs-paths config)]
    (when-not (every? fs/exists? idx-paths)
      (download-idxs config))
    (apply ->repo (local-idxs-paths config))))

(defn download-apk
  [repo app-id]
  (fs/mkdirs (config :local-apks-path))
  (let [filename (utils/apk-path app-id)
        url (apk-url repo app-id)]
    (if url
      (do
        (log/info "==> downloading " app-id)
        (utils/download url filename)
        :ok)
      (throw (Exception. (str  "could not find url for" app-id))))))
