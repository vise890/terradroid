(ns terradroid.apk
  (:require [taoensso.timbre :as log]
            [terradroid.utils :as u]
            [clojure.java.shell :as sh]
            [me.raynes.fs :as fs]))

(defn path
  [x]
  (cond
    (fs/file? x) (.getAbsolutePath x)
    (string? x) x))

(defn install-apk
  [apk-path]
  {:pre [(some? apk-path)]}
  (sh/sh "adb" "install" "-r" (path apk-path)))

(defn install-app
  [app-id]
  {:pre [(string? app-id)]}
  (install-apk (u/apk-path app-id)))
