(defproject terradroid "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/data.xml "0.0.8"]
                 [org.clojure/data.zip "0.1.3"]
                 [medley "1.2.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [me.raynes/fs "1.4.6"]]
  :main ^:skip-aot terradroid.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
